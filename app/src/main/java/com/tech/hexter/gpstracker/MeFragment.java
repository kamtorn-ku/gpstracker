package com.tech.hexter.gpstracker;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeFragment extends Fragment {

    String mUserprofileUrl;
    FirebaseAuth mAuth;

    FirebaseAuth.AuthStateListener mAuthListener;
    private static int backButtonCount;

    FirebaseUser userfirebase = FirebaseAuth.getInstance().getCurrentUser();


    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
    String myUserID = userfirebase.getUid();

    public MeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_me, container, false);

        ImageView imageProfile= (ImageView) view.findViewById(R.id.profile_image);


        try {

            mUserprofileUrl = userfirebase.getPhotoUrl().toString();

        } catch(Exception e) {

        }

//        if (mUserprofileUrl == null) {
//            imageProfile.setImageDrawable(ContextCompat.getDrawable(R.+drawable.boy));
//
//        }



        return view;
    }




}
