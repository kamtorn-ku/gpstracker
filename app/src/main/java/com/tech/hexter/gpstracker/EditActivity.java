package com.tech.hexter.gpstracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class EditActivity extends AppCompatActivity {



    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    FirebaseUser userfirebase = FirebaseAuth.getInstance().getCurrentUser();


    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

    Button buttonsave;
    Button buttonexit;
    Button buttonDelete;
    Button buttonScan;
    EditText nameText;
    EditText idText;

    String name;
    String id;

    @Override
    protected void onStart() {
        super.onStart();
       // mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("toname");
            id = bundle.getString("touid");
        }

        initFirebase();

        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonexit = (Button) findViewById(R.id.buttonexit);
        buttonsave = (Button) findViewById(R.id.buttonSave);
        nameText = (EditText) findViewById(R.id.editTextName);
        idText = (EditText) findViewById(R.id.editTextID);
        buttonScan = (Button) findViewById(R.id.buttonScanEdit);

        nameText.setText(name);
        idText.setText(id);



        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                startActivityForResult(intent, 888);

            }
        });

        buttonexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder =
                        new AlertDialog.Builder(EditActivity.this);
                builder.setMessage("Are you sure you want to delete this device ?");
                builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            Main2aActivity main = new Main2aActivity();
                            main.deleteUser(id);
                        }
                        catch (Exception e){
                            Toast.makeText(EditActivity.this,"Unable to load Data",Toast.LENGTH_SHORT).show();
                        }

                        finish();


                    }
                });
                builder.setNegativeButton("No,not yet", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();




            }
        });
        buttonsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Main2aActivity main = new Main2aActivity();
                main.updateUser(idText.getText().toString(),nameText.getText().toString());
                finish();
            }
        });




    }

    private void addEventFirebaseListener() {

        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    mDatabase.child(id).removeValue();
                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void initFirebase() {
        FirebaseApp.initializeApp(this);
//        mFirebaseDatabase = FirebaseDatabase.getInstance();
//        mDatabaseReference  = mFirebaseDatabase.getReference();
    }

//    private void deleteUser(String id){
//
//        finish();
//
//    }

    private void updateUser(User user) {
        mDatabase.child("users").child(user.getUid()).child("name").setValue(user.getName());
        mDatabase.child("users").child(user.getUid()).child("email").setValue(user.getId());

    }

//    private void createUser(String name,String id) {
//        User user = new User(UUID.randomUUID().toString(),name,id);
//        mDatabase.child("users").child(user.getId()).setValue(user);
//        // clearEditText();
//
//    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {


        if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            idText.setText(contents);
        }



    }
}
