package com.tech.hexter.gpstracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;


public class Main2aActivity extends AppCompatActivity {

    private MapView mMapView;
    private GoogleMap googleMap;
    Button addDevice;
    FrameLayout fragment1;
    Random rand = new Random();

    private ListView list_data;
    private ProgressBar circular_progress;

    String mUserprofileUrl;


    //private FirebaseDatabase mFirebaseDatabase;
   // private DatabaseReference mDatabaseReference;
    //FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    private List<User> list_users = new ArrayList<>();

    private User selectedUser;

    FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    FirebaseAuth.AuthStateListener mAuthListener;
    private static int backButtonCount;

    FirebaseUser userfirebase = FirebaseAuth.getInstance().getCurrentUser();


    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
    String myUserID = userfirebase.getUid();
    String num;





    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {

                case R.id.navigation_dashboard:
                    showfragment();
                    loadFragment(new MapFragment());
                    return true;

                case R.id.navigation_home:
                    closefragment();


                    return true;

                case R.id.navigation_notifications:
                    showfragment();
                    loadFragment(new MeFragment());

                    return true;
            }
            return loadFragment(fragment);
        }
    };
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);



    }





    @Override
    public void onBackPressed()
    {


        if(backButtonCount >= 1)
        {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2a);



        int  n = rand.nextInt(50) + 1;
        num = Integer.toString(n);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();




        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragment1 = (FrameLayout) findViewById(R.id.fragment_container);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if(firebaseAuth.getCurrentUser() == null){
                    startActivity(new Intent(Main2aActivity.this,MainActivity.class));

                }

            }
        };

        if (ActivityCompat.checkSelfPermission((Activity)this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);
        }
        circular_progress = (ProgressBar)findViewById(R.id.circular_progress);
        list_data = (ListView)findViewById(R.id.list_data);
        list_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User user = (User)adapterView.getItemAtPosition(i);
                selectedUser = user;
                String uidtest = selectedUser.getUid().toString();

                // send to editpage

                Intent intent = new Intent(view.getContext(),EditItemActivity.class);
                intent.putExtra("Uid",uidtest);
                startActivity(intent);


            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Boolean showmap = false;
            String name = bundle.getString("name");
            String id = bundle.getString("id");

            createUser(name,id,0,0);



        }


        initFirebase();
        addEventFirebaseListener();

        showfragment();
        loadFragment(new MeFragment());




    }

    private void addEventFirebaseListener() {

        circular_progress.setVisibility(View.VISIBLE);
        list_data.setVisibility(View.INVISIBLE);

        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(list_users.size() > 0)
                    list_users.clear();
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    User user = postSnapshot.getValue(User.class);
                    list_users.add(user);
                }
                ListViewAdapter adapter = new ListViewAdapter(Main2aActivity.this,list_users);
                list_data.setAdapter(adapter);

                circular_progress.setVisibility(View.INVISIBLE);
                list_data.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void initFirebase() {
        FirebaseApp.initializeApp(this);
//        mFirebaseDatabase = FirebaseDatabase.getInstance();
//        mDatabaseReference  = mFirebaseDatabase.getReference();
    }

    public boolean loadFragment(Fragment fragment){

        if(fragment != null){

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

            return true;
        }
        return false;
    }

    private void closefragment() {
        fragment1.setVisibility(View.INVISIBLE);

    }
    public void showfragment() {
        fragment1.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main,menu);
    return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == R.id.menu_add)
//        {
//            createUser();
//        }
//        else if(item.getItemId() == R.id.menu_save)
//        {
//            User user = new User(selectedUser.getUid(),input_name.getText().toString(),input_email.getText().toString());
//            updateUser(user);
//        }
//        else if(item.getItemId() == R.id.menu_remove){
//            deleteUser(selectedUser);
//        }
      return true;
    }

    public void deleteUser(String id) {
        try {
            mDatabase.child("users").child(id).removeValue();
        }
        catch (Exception e){
            Toast.makeText(Main2aActivity.this,"Unable to load",Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUser(String id,String name) {
        mDatabase.child("users").child(id).child("name").setValue(name);
        mDatabase.child("users").child(id).child("id").setValue(id);
        clearEditText();
    }

    private void createUser(String name,String id,double lat,double lng) {
          User user = new User(UUID.randomUUID().toString(),name,id,lat,lng);
          mDatabase.child("users").child(user.getId()).setValue(user);
         // clearEditText();

    }

    private void clearEditText() {
//        input_name.setText("");
//        input_email.setText("");
    }




}
