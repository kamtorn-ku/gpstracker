package com.tech.hexter.gpstracker;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SelectScanActivity extends AppCompatActivity {

    Button buttonback;
    Button ScanQr;
    Button complete;
    TextView textresult;
    EditText editTextID;
    EditText editTextName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_scan);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        if (ActivityCompat.checkSelfPermission((Activity) this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this, new String[]{
                    Manifest.permission.CAMERA
            }, 10);
        }

        buttonback = (Button) findViewById(R.id.buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();


            }
        });

        ScanQr = (Button) findViewById(R.id.buttonQr);
        ScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                startActivityForResult(intent, 999);


            }
        });

        textresult = (TextView) findViewById(R.id.textView2);
        editTextID = (EditText) findViewById(R.id.editTextID);
        editTextName = (EditText) findViewById(R.id.editTextName);
        complete = (Button) findViewById(R.id.buttonComplete);

        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String id = editTextID.getText().toString();
                String name = editTextName.getText().toString();


                Intent intent = new Intent(SelectScanActivity.this, Main2aActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("id", id);
                startActivity(intent);
//                }
//                else
//                    {
//                    AlertDialog.Builder builder =
//                            new AlertDialog.Builder(SelectScanActivity.this);
//                    builder.setMessage("No device found.");
//                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//
//
//                        }
//                    });
//                    builder.show();
//
//                }




            }
        });


    }



    public void onActivityResult(int requestCode, int resultCode, Intent intent) {


        if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            editTextID.setText(contents);
        }



    }
}