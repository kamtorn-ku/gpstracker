package com.tech.hexter.gpstracker;

/**
 * Created by BlueGalex on 2/28/2018.
 */

public class User {
    private String uid,name,id;
    double lat;
    double lng;

    public User() {
    }

    public User(String uid, String name, String id,double lat,double lng) {
        this.uid = uid;   // Primary key and key
        this.name = name;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
       // this.buzzer = buzzer;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

//    public Boolean getBuzzer(){
//        return buzzer;
//    }
//
//    public void setBuzzer(Boolean buzzer){
//        this.buzzer = buzzer;
//    }
    public void setId(String id) {
        this.id = id;
    }

    public double getLat(){
        return lat;
    }
    public void setLat(){
        this.lat = lat;
    }
    public double getLng(){
        return lng;
    }
    public void setLng(){
        this.lng = lng;
    }




}
