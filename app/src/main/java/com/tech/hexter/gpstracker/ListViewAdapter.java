package com.tech.hexter.gpstracker;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by BlueGalex on 2/28/2018.
 */

public class ListViewAdapter extends BaseAdapter {

    Activity activity;
    List<User> lstUsers;
    LayoutInflater inflater;
    private List<User> list_users = new ArrayList<>();
    private User selectUser;
    Context mContext;


    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");



    public ListViewAdapter(Activity activity, List<User> lstUsers) {
        this.activity = activity;
        this.lstUsers = lstUsers;
    }

    @Override
    public int getCount() {
        return lstUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return lstUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        inflater = (LayoutInflater)activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        if(view == null) {
            view = inflater.inflate(R.layout.listview_item, null);
        }


        TextView txtUser = (TextView)view.findViewById(R.id.textViewNameDevice);
        //TextView txtEmail = (TextView)itemView.findViewById(R.id.list_email);
        //try {

            String name = lstUsers.get(i).getName();

            txtUser.setText(name);
            //txtEmail.setText(lstUsers.get(i).getEmail());
//        }
//        catch (Exception e){
//            Toast.makeText(view.getContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
//        }


        Button btnGetlocartion = view.findViewById(R.id.button_getLocation);
           Button btnGetsound = view.findViewById(R.id.button_getSound);
           Button btnGetedit = view.findViewById(R.id.button_getEdit);


        btnGetedit.setTag(i);

           btnGetsound.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   try {
//                       int position = (Integer) view.getTag();
//                       User user = (User) getItem(position);
//
//                       String id = user.getId();


                       mDatabase.child("users").child("ITMIRACLE01").child("buzzer").setValue(true);

                       Toast.makeText(view.getContext(),"Sending Beacon to Device.....",Toast.LENGTH_LONG).show();
                   }
                   catch (Exception e){

                   }






               }
           });

           btnGetedit.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {

                    int position = (Integer) view.getTag();
                    User user = (User) getItem(position);

                    String ownuid = user.getId();
                    String name = user.getName();

                    Intent intent = new Intent(view.getContext(),EditActivity.class);
                    intent.putExtra("touid",ownuid);
                    intent.putExtra("toname",name);
                    view.getContext().startActivity(intent);



               }
           });


           btnGetlocartion.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {


//                try {
//                    int position = (Integer) view.getTag();
//                    User user = (User) getItem(position);
//
//                    double lat = user.getLat();
//                    double lng = user.getLng();
//                    String name = user.getName();
//
//                    Bundle bundle = new Bundle();
//
//                    bundle.putDouble("lat", lat);
//                    bundle.putDouble("lng", lng);
//                    bundle.putString("name", name);
//                    MapFragment mapFragment = new MapFragment();
//                    mapFragment.setArguments(bundle);
//                }
//                catch (Exception e){
//                    Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_SHORT).show();
//                }
//
               }
         });


        return view;
    }








}
