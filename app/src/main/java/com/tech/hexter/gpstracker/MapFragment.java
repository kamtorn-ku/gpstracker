package com.tech.hexter.gpstracker;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MapFragment extends Fragment {

    private MapView mMapView;
    private GoogleMap googleMap;
    Context context;
    private Location mLastLocation;
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

    double latitude =14.044677;
    double longitude= 100.733387;

    double lat=0;
    double lng=0;
    String name;



    @Override
    public void onStart() {
        super.onStart();



    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View inflated = inflater.inflate(R.layout.fragment_map, container, false);




//        try {
//            lat = getArguments().getDouble("lat");
//            lng = getArguments().getDouble("lng");
//            name = getArguments().getString("name");
//        }
//        catch (Exception e){
//
//            Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
//        }

        mMapView = (MapView) inflated.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    lat = dataSnapshot.child("users").child("ITMIRACLE01").child("lat").getValue(Double.class);
                    lng = dataSnapshot.child("users").child("ITMIRACLE01").child("lng").getValue(Double.class);
                    name = dataSnapshot.child("users").child("ITMIRACLE01").child("name").getValue(String.class);



                        mMapView.getMapAsync(new OnMapReadyCallback() {


                            @Override
                            public void onMapReady(GoogleMap mMap) {
                                googleMap = mMap;

                                googleMap.clear();

                                // For showing a move to my location button


                                googleMap.setMyLocationEnabled(true);

                                if(lat != 0 && lng!=0) {

                                LatLng Test1 = new LatLng(lat, lng);


                                MarkerOptions marker = new MarkerOptions().position(Test1).title(name);
                                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.luggagepin));

                                googleMap.addMarker(marker);


                                // For dropping a marker at a point on the Map
//                LatLng sydney = new LatLng(14.044677,100.733387);
//                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));
//
//
//                LatLng sydney2 = new LatLng(14.045180, 100.733259);
//                googleMap.addMarker(new MarkerOptions().position(sydney2).title("Marker Title2").snippet("Marker Description"));
                                // For zooming automatically to the location of the marker
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(Test1).zoom(12).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                }
                                else {
                                    Toast.makeText(getContext(),"No device Connect to Server",Toast.LENGTH_SHORT).show();
                                }


                            }
                        });


                }catch (Exception e){
                    //Toast.makeText(getContext(),"No Device Tracker",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {



            }
        });






        return inflated;
    }

    @Override
    public void onResume() {
        super.onResume();




    }
}