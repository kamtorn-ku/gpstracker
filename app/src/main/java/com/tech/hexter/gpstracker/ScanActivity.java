package com.tech.hexter.gpstracker;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    Context context;
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();                             // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause

    }


    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Toast.makeText(getApplicationContext(),rawResult.getText(), Toast.LENGTH_SHORT).show();
        mScannerView.stopCamera();

        Intent returnIntent = new Intent();

        returnIntent.putExtra("SCAN_RESULT",rawResult.getText());

        setResult(RESULT_OK,returnIntent);
        finish();


//        // If you would like to resume scanning, call this method below:
//        mScannerView.resumeCameraPreview(this);
    }
}
